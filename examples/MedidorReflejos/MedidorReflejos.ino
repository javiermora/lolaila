//////////////////////////////////////////////////////////
// Medidor de reflejos con la Placa de Fundamentos de Electronica
#include <Wire.h>
#include <Lolaila.h>

unsigned long startTime;
unsigned long lapse = 0;
char digitToDisplay = 0;
volatile enum {STOPPED, IN_START, RUNNING, CHEATING} state;

void setup() {
  setupDisplay();
  setupRgb();
  state = STOPPED;
}

void loop() {
  if (state == STOPPED || state == CHEATING) {
    displayValueAndWait(lapse);
    start();
  }

  if (state == IN_START) {
    delay(300);
    while (millis() < startTime) {
      checkFinishButton();
    }
    if (state == IN_START) {
      state = RUNNING;
    }
  }

  if (state == RUNNING) {
    checkFinishButton();
  } 
   
}

void start() {
  lapse = 0;
  state = IN_START;
  startTime = millis() + 2000 + random(1000);
}
  
void checkFinishButton() {
  if (digitalRead(2) == HIGH) {
    if (state == IN_START) {
      state = CHEATING;
      lapse = 0;
      sayNoNoNo();
    } else {
      if (state == RUNNING) {
        state = STOPPED;
        lapse = millis() - startTime;
        sayYes();
      }
    }
  }
  setStateRgbCode();
}

void setStateRgbCode() {
  switch(state)  {
    case STOPPED  : setRgb(  0,   0,   0); break;
    case IN_START : setRgb(  0,   0, 255); break;
    case RUNNING  : setRgb(  0, 255,   0); break;
    case CHEATING : setRgb(255,   0,   0); break;
 
    default       : setRgb(  0,   0,   0); break;
  }
}


void displayValueAndWait(int n) {
  // Mostramos timepo en centesimas de segundo (viene en mil?simas)
  n = n / 10;
  n = constrain(n, 0, 99); // lo limitamos entre 0 y 99
  
  // y lo mostramos mientras no se pulse el boton BT2
  while( digitalRead(3) == LOW ) {
    setDisplayDigit(0, n % 10);
    setDisplayDigit(1, n / 10);
  }
  setDisplayDigit(0, '-');
  delay(1000);
}

void sayNoNoNo() {
  playTone(1000, 20);
  delay(100);
  playTone(1000, 20);
  delay(100);
  playTone(1000, 20);
  delay(100);
}

void sayYes() {
  playTone(1000, 20);
  delay(20);
}
