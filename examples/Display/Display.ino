//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electrónica
#include <Wire.h>
#include <Lolaila.h>

void setup() {
  setupDisplay();
}

void loop() {  
  setDisplayDigit(0, 2);
  setDisplayDigit(1, 5);
}

