//////////////////////////////////////////////////////////
// Test Placa de Fundamentos de Electr?nica
#include <Wire.h>
#include <Lolaila.h>

void setup() {
  setupAccel();
  Serial.begin(9600);
}

int aX;
void loop() {  
  aX = map(getAccel(X), -512, 512, 0, 255);

  if (aX > 230 ) {
    Serial.write((char)'B');
    delay(50);
  }

  if (aX < 25 ) {
    Serial.write((char)'C');
    delay(50);
  }
}

