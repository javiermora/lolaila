//////////////////////////////////////////////////////////
// Placa de Fundamentos de Electronica
#define startTone(A)    ((A) == 0 ? noTone(11) : tone(11, (A)))
#define stopTone()      noTone(11)
#define playTone(A, B)  ( ((A) == 0 || (B) == 0) ? noTone(11) : toneDelay((A), (B)) )
#define Accel_X 1
#define Accel_Y 0
#define Accel_Z 2
#define getAccelX()     getAccel(Accel_X) 
#define getAccelY()     getAccel(Accel_Y) 
#define getAccelZ()     getAccel(Accel_Z) 
#define getMeanAccelX()     getMeanAccel(Accel_X) 
#define getMeanAccelY()     getMeanAccel(Accel_Y) 
#define getmeanAccelZ()     getMeanAccel(Accel_Z) 


//////////////////////////////////////////////////////////
// RGB Section
void setupRgb();
void setRgb(byte r, byte g, byte b);
void setRgb(unsigned long int rgb);

//////////////////////////////////////////////////////////
// Display Test Section
void setupDisplay();
void setDisplayDigit(byte n, byte val);
void setDisplaySegments(byte n);

//////////////////////////////////////////////////////////
// Accelerometer Test Section
void setupAccel();
int getAccel(int axis);
int getMeanAccel(int axis);

//////////////////////////////////////////////////////////
// Tone
void toneDelay(int a, int b);

unsigned long int color1D(float c);
