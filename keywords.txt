#######################################
# Syntax Coloring Map For Gominola
#######################################


#######################################
# Methods and Functions (KEYWORD2)
#######################################

startTone	KEYWORD2
stopTone 	KEYWORD2
playTone	KEYWORD2
getAccelX 	KEYWORD2
getAccelY	KEYWORD2
getAccelZ	KEYWORD2
getMeanAccelX 	KEYWORD2
getMeanAccelY	KEYWORD2
getMeanAccelZ	KEYWORD2
setupRgb 	KEYWORD2
setRgb 		KEYWORD2
setupDisplay 	KEYWORD2
setDisplayDigit KEYWORD2
setDisplaySegments KEYWORD2
setupAccel 	KEYWORD2
getAccel 	KEYWORD2
getMeanAccel 	KEYWORD2

######################################
# Constants (LITERAL1) 
#######################################

X LITERAL1
Y LITERAL1
Z LITERAL1
